import App from './App'
import store from './store/_vuex/index.js' //vuex
import config from '@/app.config.js' //全局配置文件
import locales from '@/locale/index.js' //自定义国际化配置
//引入wy 
import wy from '@/uni_modules/wy-core';
import vkOS from '@/uni_modules/vk-unicloud'; //选填，如果不是vk可以不引用
// #ifndef VUE3 
import VueI18n from 'vue-i18n'
import Vue from 'vue'  
Vue.use(wy); 
let {locale}= Vue.prototype.wy.init({
  Vue,               // Vue实例
  config,	           // 配置
  locales,
  store,
  vkOS
});
Vue.use(VueI18n)
const i18n = new VueI18n(locale)
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  i18n,
  store, 
  ...App
});

app.$mount();
// #endif

// #ifdef VUE3
import { createI18n } from 'vue-i18n'
import { createSSRApp } from 'vue'
export function createApp() {
  const app  = createSSRApp(App)  
  // 引入wy
  app.use(wy); 
 let {locale}=  app.config.globalProperties.wy.init({
    Vue: app,          // Vue实例
    config,	locales
    ,store           // 配置
    ,vkOS
  });  
  // 引入vuex
  app.use(store)
  const i18n = createI18n(locale)
  app.use(i18n)
  return { app }
}
// #endif