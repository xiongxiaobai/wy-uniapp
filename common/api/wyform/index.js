var modules={}
// #ifdef VUE2
const modulesFiles = require.context('./js_sdk', true, /\.js$/);
modulesFiles.keys().map((modulePath, index) => {
	const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	const value = modulesFiles(modulePath)
	modules[moduleName] = value.default
	return modules
});
// #endif

// #ifdef VUE3
//const modulesFiles3 = import.meta.globEager('./js_sdk/*.js');
const modulesFiles3 = import.meta.glob('./js_sdk/*.js',{ eager: true })
for (const modulePath in modulesFiles3) {
	const moduleName = modulePath.replace('js_sdk/','').replace(/^\.\/(.*)\.\w+$/, '$1').replace('js_sdk/','')
	modules[moduleName] = modulesFiles3[modulePath].default;
}
// #endif
 
export default modules