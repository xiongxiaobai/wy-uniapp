import {getWy,getPlatform} from '@/uni_modules/wy-core/context'
import {getModel,getList,doAction} from '../utils.js'
const that={
	_res(result){  //结果包装
		let {ret,msg,data}=result.data
		result.data={
			code:ret==200?0:1,
			data,
			msg
		}
		return  result
	},
	_res1(result){  //结果包装
		let {data,msg}=result
		let {status,tip}=data
		result={
			code:status==1?0:1,
			msg:tip?tip:msg, 
			data:data
		}
		return result
	},
	getUserInfo(info){
		let {profile,group}=info
		//let {uid,username}=basic
		let {avatar,uid,username}=profile
		var userInfo={
			_id:uid, 
			avatar:avatar?avatar:'',
			...profile,
			group, 
		} 
		return userInfo
	},
	pub_login(option){
        console.log('debug','----pub_login')
		let {username,password}=option.data
		let obj={
			data:{
				username,
				password,
				pre:getPlatform()
			},
			params:{
				showSession:1,
				type: 'user',
				ac:'Login'
			},
			option
		}
		return getWy().request(obj) 
	}, 
	pub_loginByWeixin(option){
		let {code,userData}=option.data
		let data={
			code,userData 
		} 
		let obj={
			data ,
			params:{
				showSession:1,
				type: 'wechat',
				ac:'Login',
				ak:getWy().config.App.package,
				pre:getPlatform()
			},
			res:that.pub_login_,
			option
		}
		return getWy().request(obj) 
	},
	pub_loginBySms(option){
		let {mobile,code,type,password}=option.data
		let data={
			tel:mobile,
			code,
			pre:getPlatform(),
			
		}
		if(password){
			data={
				...data,
				password
			}
		}
		let obj={
			data ,
			params:{
				showSession:1,
				type: 'tel',
				ac:'Login',
				item:type?type:'login'
			},
			res:that.pub_login_,
			option
		}
		return getWy().request(obj) 
	},
	pub_login_(res){
	    var wy=getWy()
	    res=that._res1(res)
		let {code,data}=res
		if(code==0){
			//拼接
			let {sid,uid,username,info}=data
			const tokenExpired=(wy.pubfn.unix()+60*60*24*30)*1000
			var userInfo=that.getUserInfo(info)
			let obj={
				code,
				msg:'登录成功',
				needUpdateUserInfo: true,
				token:sid,
				tokenExpired,
				type:'login',
				uid,
				username,
				vk_uni_token:{
					token:sid,
					tokenExpired
				},
				userInfo
			}
			
			res=obj
		} 
		return res 
	}, 
	pub_userSession(option){
		return getModel('UserSession',option) 
		 
	},
	pub_logout(option){
		var wy=getWy()
		let {sid}=option.data
		let obj={
			data:{
				sid
			},
			res:that._res1,
			params:{
				ac:'LoginOut'
			},
			option
		}
		wy.callFunctionUtil.deleteToken()
		return wy.request(obj) 
	}, 
	kh_updateUser(option){
		let obj={
			data:option.data,
			params:{
				ac:'UserProfileEidt',
				showSession:1
			}, 
			option
		} 
		return getWy().request(obj) 
	},
	kh_updateUser_(res){
		var wy=getWy()
		res=that._res1(res)
		let {code,data,msg}=res
		if(code==0){
			let {info}=data
			var userInfo=that.getUserInfo(info)
			res={
				code,msg,
				needUpdateUserInfo:true,
				userInfo
			}
		}
		
		return res
	},
	kh_setAvatar(option){
		let {avatar}=option.data
		let obj={
			data:{
				avatar
			},
			params:{
				ac:'UserProfileEidt',
				showSession:1
			}, 
			res:that.kh_updateUser_,
			option
		} 
		return getWy().request(obj) 
	},
	kh_setUserName(option){
		let {username}=option.data
		let obj={
			data:{
				username
			},
			params:{
				ac:'UserLoginEdit',
				showSession:1
			}, 
			res:that.kh_updateUser_,
			option
		} 
		return getWy().request(obj) 
	},
	kh_resetPwd(option){
		let {password,username}=option.data
		let obj={
			data:{
				username,
				password
			},
			params:{
				ac:'UserLoginEdit',
				showSession:1
			}, 
			res:that.kh_updateUser_,
			option
		} 
		return getWy().request(obj) 
	},
	pub_sendSmsCode(option){
		let {mobile,type}=option.data
		let obj={
			data:{
				tel:mobile,
				item:type?type:'login'
			},
			params:{
				ac:'SendSms' 
			}, 
			res:that._res1,
			option
		} 
		return getWy().request(obj) 
	},
	kh_userAccount(option){
		return getModel('Account',option) 
	},
	pub_userView(option){
		return getModel('UserView',option) 
	},
	pub_userNavData(option){
		return getList('UserNavData',option) 
	},
	pub_userFollow(option){
		return getList('UserFollow',option) 
	},
	pub_license(option){
	 	return getList('License',option) 
	}
	
	
};
export default that