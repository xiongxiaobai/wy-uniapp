import {getModel,getList,getData,doAction} from '../utils.js'
const that={ 
	pub_list(option){
		let {ac}=option
		return getList(ac,option)     
	},
	kh_list(option){
		let {ac}=option
		return getList(ac,option)       
	},
    pub_action(option){
    	let {ac}=option
    	return doAction(ac,option)     
    },
	kh_action(option){ 
		let {ac}=option
		return doAction(ac,option)     
	},
	pub_model(option){
		let {ac}=option
		return getModel(ac,option)     
	},
    kh_model(option){
    	let {ac}=option
    	return getModel(ac,option)     
    },
	pub_data(option){
		let {ac}=option
		return getData(ac,option)     
	},
    kh_data(option){
    	let {ac}=option
    	return getData(ac,option)     
    }
};
export default that