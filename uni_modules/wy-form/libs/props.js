
export const wyProps={
    options: {// 微信小程序中 options 选项
        virtualHost: true,
    },
    inject: {
        wyPage: {
        	default () {
        		return null
        	}
        },
    },
    data() {
    	return {
            wyTempProps:{
               screenSize:'', 
            },
            
        }
    },
    mounted() {   
        // #ifdef VUE3
        var name=this.$.type.name
        var uuid=this.autoUuid?this.autoUuid:this.$.uid
        // #endif
        // #ifndef VUE3
        var name=this.$options._componentTag
        var uuid=this.autoUuid?this.autoUuid:this._uid
        // #endif   
        if(this.wyPage &&this.autoPageRegister&& (this.autoPage||(this.wyTemp&&this.wyTemp.autoPage))){
            this.wyPage.register(this._getInstance,name,uuid,this._pageCallback)
        } 
        if(this._mounted){
            this._mounted()
        }  
    },
    methods:{
        _pageCallback(e){
            let {event,data}=e
            if(event=='screen'){
                this.wyTempProps.screenSize=data
            }
            if(this.pageCallback){
                this.pageCallback(e)
            }
        },
        _getInstance(){
            if(this.getInstance){
                return this.getInstance()
            }else{
              return this  
            } 
        }  
    },
    computed:{ 
        ScreenSize(){
            return this.wyTempProps.screenSize?this.wyTempProps.screenSize:this.screenSize
        }
    },
    props: {
        autoUuid:{
            type:String,
            default:''
        },
        autoPageRegister:{
            type:Boolean,
            default:true
        },
        //自动从wy-page中同步size
        autoPage:{
            type:Boolean,
            default:false
        },
        //主题
    	themeName:{
    		type:String,
    		default:'' 
    	},
        //small,medium,wide 对应手机，平板，pc
        screenSize:{
        	type:String,
        	default:'small' 
        },
        //数据源
    	dataSource:{
    		type:Object,
    		default:()=>{
    			return{
    			}
    		}
    	}
    },
}