const SUCCESS_CODE=0; //服务端请求成功状态的code值
const DEFAULT_HEADER={} //默认header
const util={
	_resList(result){  //结果包装
		let {code,ret,msg,data,preload}=result
        const isSuccess=preload|| code==SUCCESS_CODE
		result={
			code:isSuccess?0:1,
			data,
			msg,
			ret:code
		}
		return  result
	},
	_resModel(result){  //结果包装
		let {code,msg,data,preload}=result
        const isSuccess=preload||code==SUCCESS_CODE
		result={
			code:isSuccess?0:1,
			data,
			msg,
			ret:code
		}
		return  result
	},
	_resData(result){  //结果包装
		//let {data}=result
		return  result
	},
	_resAction(result){  //结果包装
		let {code,data,msg,preload}=result
        const isSuccess=preload||code==SUCCESS_CODE
        code=isSuccess?1:0
		result={
			code:isSuccess?0:1,
			msg,
			data,
			ret:code
		}
		return result
	},
	
}
export function getData(_ac,option,_params={}){
    let {params,data={},method,header}=option
    header=header?header:DEFAULT_HEADER
    method:method?method:'POST'
    let obj={
        data, 
        _ac,
        params:{ 
            ...params,
            ..._params
        },
        method,
        header,
        res:util._resData,
        option
    }
    return uni.wy.request(obj)
}
export function	getModel(_ac,option,_params={}){
    let {params,data={},method,header}=option
    header=header?header:DEFAULT_HEADER
    method:method?method:'POST'
    let obj={
        data,
        _ac,
        params:{ 
            ...params,
            ..._params
        },
        method,
        header,
        res:util._resModel,
        option
    }
    return uni.wy.request(obj)
}
export function	getList(_ac,option,_params={}){
    let {params,data={},method,header}=option
    header=header?header:DEFAULT_HEADER
    method:method?method:'POST'
    let obj={
        data,
        _ac,
        params:{ 
            ...params,
            ..._params
        },
        method,
        header,
        res:util._resList,
        option
    }
    return uni.wy.request(obj)
}
export function	doAction(_ac,option,_data={},_params={}){
    let {params,data,method,header}=option
    header=header?header:DEFAULT_HEADER
    method:method?method:'POST'
    let obj={
        data:{
            ...data,
            ..._data
        },
        _ac,
        params:{ 
            ...params,
            ..._params
        },
        method,
        header,
        res:util._resAction,
        option
    }
    return uni.wy.request(obj)
}