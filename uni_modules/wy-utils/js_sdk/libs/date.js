import {isNumber} from './types'
/**
 * 必填
 * @param {String} key
 */
export const requered = (key='') => {
  console.error(`缺少参数${key}`)
}
/**
 * 格式化时间
 * @param {any} value
 * @param {String} format
 * @example utilscore.formatTime('2019/06/04 12:45:32','YYYY~MM~DD hh~mm~ss 星期W  季度Q') // => "2019~06~04 12~45~32 星期二  季度2"
 */
export const formatTime = (value, format) => {
  if(isNumber(value)&&value.toString().length==10){
	  value=value*1000
  }
  let nowDate = new Date(value)
  let weeks = ['日', '一', '二', '三', '四', '五', '六']
  let time = (new Date(+nowDate + 8 * 3600 * 1000)).toISOString().substr(0, 19).replace(/[a-z]/i, ' ');
  let [_, YYYY, MM, DD, hh, mm, ss] = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/g.exec(time)
  var filterTime = (type, _) => type.slice(0, _.length)
  return format.replace(/(Y{1,4})/g, ($1) => filterTime(YYYY, $1))
    .replace(/(M{1,2})/g, ($1) => filterTime(MM, $1))
    .replace(/(D{1,2})/g, ($1) => filterTime(DD, $1))
    .replace(/(h{1,2})/g, ($1) => filterTime(hh, $1))
    .replace(/(m{1,2})/g, ($1) => filterTime(mm, $1))
    .replace(/(s{1,2})/g, ($1) => filterTime(ss, $1))
    .replace(/(W{1})/g, ($1) => weeks[nowDate.getDay()])
    .replace(/(Q{1})/g, ($1) => Math.floor((nowDate.getMonth() + 3) / 3))
  }


  /**
   * @param  {s} 秒数
   * @return {String} 字符串
   * @example utilscore.formatHMS(3610) // -> 1h0m10s
   */
  export const formatHMS = (s) => {
    var str = ''
    if(s > 3600 * 24) {
      str = Math.floor(s / 3600 / 24) + 'd' + Math.floor(s / 3600 % 24) + 'h' + Math.floor(s % 3600 / 60) + 'm' + s % 60 + 's'
    }
    else if (s > 3600) {
      str = Math.floor(s / 3600) + 'h' + Math.floor(s % 3600 / 60) + 'm' + s % 60 + 's'
    } else if (s > 60) {
      str = Math.floor(s / 60) + 'm' + s % 60 + 's'
    } else {
      str = s % 60 + 's'
    }
    return str
  }
export const get_unix_time=(dateStr)=> {
    var newstr = dateStr.replace(/-/g, '/');
    var date = new Date(newstr);
    var time_str = date.getTime().toString();
    return time_str.substr(0, 10);
}
  /**
   * 获取时间戳 (秒)
   * @param {any} value
   */
  export const unix = (value) => {
    if(value===void 0) return unix(Date.now())
    return Math.floor(new Date(value).getTime() / 1000);
  }
	export const longunix = (value) => {
    if(value===void 0) return unix(Date.now())
    return Math.floor(new Date(value).getTime());
  }
  
  export const formatSeconds=(value) =>{ 
    var theTime = parseInt(value); // 秒
    var middle = 0; // 分
    var hour = 0; // 小时

    if (theTime > 60) {
      middle = parseInt(theTime / 60);
      theTime = parseInt(theTime % 60);
      if (middle > 60) {
        hour = parseInt(middle / 60);
        middle = parseInt(middle % 60);
      }
    }
    var result = "" + parseInt(theTime) + "秒";
    if (middle > 0) {
      result = "" + parseInt(middle) + "分" + result;
    }
    if (hour > 0) {
      result = "" + parseInt(hour) + "小时" + result;
    } 
    return result
  }
  
  /**
   * 倒计时
   * @param {any} target
   */
  export const countDown = (target=requered()) => {
    let time = unix(target) - unix()
    return formatHMS(time)
  }
  export const  formatMsgTime = function (dateTimeStamp,now=null) {
 //function getDateDiff(dateTimeStamp){
	 if(dateTimeStamp<10000000000){
		 dateTimeStamp=1000*dateTimeStamp
	 }
	 if(now && now<10000000000){
		  now=1000*now
	 } 
	 var result=''
 	var minute = 1000 * 60;
 	var hour = minute * 60;
 	var day = hour * 24;
 	var halfamonth = day * 15;
 	var month = day * 30;
	var year=day*365
	var px='前'
	if(!now){
		now = new Date().getTime();
	}
 	var diffValue = now - dateTimeStamp;
 	if(diffValue < 0){
		px='后'
		//return;
	}
	var yearC=diffValue/year
 	var monthC =diffValue/month;
 	var weekC =diffValue/(7*day);
 	var dayC =diffValue/day;
 	var hourC =diffValue/hour;
 	var minC =diffValue/minute;
	if(yearC>=1){
		result="" + parseInt(yearC) + "年"+px;
	}
 	else if(monthC>=1){
 		result="" + parseInt(monthC) + "月"+px;
 	}
 	else if(weekC>=1){
 		result="" + parseInt(weekC) + "周"+px;
 	}
 	else if(dayC>=1){
 		result=""+ parseInt(dayC) +"天"+px;
 	}
 	else if(hourC>=1){
 		result=""+ parseInt(hourC) +"小时"+px;
 	}
 	else if(minC>=1){
 		result=""+ parseInt(minC) +"分钟"+px;
 	}else
 	result="刚刚";
 	return result;
 }
 export const  formatMsgTime1 = function (timespan) {

    var dateTime = new Date(timespan);

    var year = dateTime.getFullYear();
    var month = dateTime.getMonth() + 1;
    var day = dateTime.getDate();
    var hour = dateTime.getHours();
    var minute = dateTime.getMinutes();
    var second = dateTime.getSeconds();
    var now = new Date();
   // var now_new = Date.parse(now.toDateString());  //typescript转换写法
  	var now_new=now.getTime()
    var milliseconds = 0;
    var timeSpanStr;

    milliseconds = now_new - timespan;

    if (milliseconds <= 1000 * 60 * 1) {
      timeSpanStr = '刚刚';
    }
    else if (1000 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60) {
      timeSpanStr = Math.round((milliseconds / (1000 * 60))) + '分钟前';
    }
    else if (1000 * 60 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24) {
      timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60)) + '小时前';
    }
    else if (1000 * 60 * 60 * 24 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24 * 15) {
      timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60 * 24)) + '天前';
    }
    else if (milliseconds > 1000 * 60 * 60 * 24 * 15 && year == now.getFullYear()) {
      timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute;
    } else {
      timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
    }
    return timeSpanStr;
  };
