export default {
	config: {
        pre:'',//前缀
		groupPre:'group__',
		key:null,
		data:null,
        detail:false,//获取原始数据
		type:'storage',
		df:null,//默认值
		time:2592000,//缓存时间
		clear:false,
		success:null
	},
    interceptor:null,
    init(_conf={},interceptor=null){
        this.config=Object.assign(this.config,_conf)
        if(interceptor){
            this.interceptor=interceptor
        } 
    },
	/**
	 * 获取缓存
	 * @param {string} key 标识
	 * @param {any} df 未取到值时的缺省值
	 */
	get(key,df=''){
        return this.S({
            key,
            df
        })
    },
	/**
	 * 批量获取缓存，返回对象
	 * @param {Array} keys 标识
	 * @param {Object} dfs 指定key的缺省值
	 * @param {any} df 未取到值时的缺省值
	 */
	gets(keys=[],dfs={},df=null){
		var obj={}
		keys.forEach((key)=>{
			var val=this.get(key,dfs.hasOwnProperty(key)?dfs[key]:df)
			obj[key]=val
		})
		return obj
	},
    detail(key){
        return this.S({
            key,
            detail:true
        })
    },
    clear(key){
        return this.S({
            key,
            clear:true 
        })
    },
	clearAll(){
		uni.clearStorageSync(); 
	},
    remove(key){
      this.clear(key) 
    },
	removes(keys=[]){
		keys.forEach((key)=>{
			this.clear(key) 
		})
	},
	removeGroup(group,nokeys=[]){
		var groupkey=this.config.groupPre+group
		var groupData=this.get(groupkey,[])
		groupData.forEach((key)=>{
			if(!nokeys.includes(key)){
				this.clear(key)
			} 
		})
	},
	setGroup(key){
		var keys=key.split('.')
		//group.key 则第一个是分组
		if(keys.length>1){ 
			var groupkey=this.config.groupPre+keys[0]
			var groupData=this.get(groupkey,[])
			if(!groupData.includes(key)){
				groupData.push(key)
				this.set(groupkey,groupData,0,true)
			} 
		}
	},
    set(key,data,time=0,isgroup=false){
        if(time==0){
            time=this.config.time
        }
		if(!isgroup){
			this.setGroup(key) 
		} 
        return this.S({
            key,
            data,
            time
        })
    },
	S(options)
	{
		let _config=Object.assign({}, this.config, options) 
		if(_config.key!=null)
		{
            let realkey=_config.key
            _config.key=_config.pre+_config.key
			
			if(this.interceptor)
			{
				this.interceptor(_config)
				if(_config.key==null)
				{
					return _config.df
				}
			}
			//清除缓存
			if(_config.clear)
			{
				if (process.env.NODE_ENV === 'development')
				{
					console.log('---remove cache----key='+realkey);
				}
				try{
					uni.removeStorage({
						key:_config.key
					})

				}catch(e){
					//TODO handle the exception
				}
			}
			//添加缓存
			else if(_config.data!=null){
				let timestamp=parseInt(new Date().getTime()/1000);
				let StorageData={
					data:_config.data,
					time:_config.time+timestamp
				};
				uni.setStorage({
					key:_config.key,
					data:StorageData,
					success(r){
						//console.log(r)
					},
					fail(err) {
					//	console.log(r)
					}

				});
				if (process.env.NODE_ENV === 'development')
				{
					console.log('---add cache----key='+realkey+',time='+(_config.time+timestamp));
				}
				_config.df=_config.data;
			}
			else
			{
				//取出缓存
				try {
					if(_config.success){
						uni.getStorage({
							key:_config.key,
							success(rs) {
							_config.success(rs.data)
							},
							fail() {
								_config.success(null)
							}

						})
						return _config.df
					}

					const value = uni.getStorageSync(_config.key); 
                    if(_config.detail){
                        return value
                    }
					if (value) {
						let timestamp=parseInt(new Date().getTime()/1000);
						if(value.data&&value.time&&value.time>timestamp)
						{
							if (process.env.NODE_ENV === 'development')
							{
								console.log('---from cache----key='+realkey+',time='+(value.time-timestamp));
							}
							_config.df=value.data; 
						}
						else
						{
							if (process.env.NODE_ENV === 'development')
							{
								console.log('---cache gq----key='+realkey);
								//options.df=null;
							}
						}
					}
					//return options.df;
				} catch (e) {
					// error
					//return options.df;
				}
			}
		}
		else
		{
			if (process.env.NODE_ENV === 'development')
			{
				console.log('the key cannot empty!');
			}
		}
		return _config.df;
	}
}
