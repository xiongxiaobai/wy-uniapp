class Stateless{
	//var Trigger=[];
	//var State=[];
	//var Configure={};
	/*
	var stateModel={
		SubstateOf:'', //指定状态的子状态
		OnEntry:()=>{},//进入当前状态时执行
		OnEntryFrom:[
			{trigger:'',action:()=>{}}//从指定触发器而来时执行
		],
		Permit:[
			{trigger:'',state:''}//当前状态下可由指定触发器转变至指定状态
		],
		OnExit:()=>{} //退出时执行
	}
	*/

	constructor(state =[],trigger =[],init=null) {
		if(init==null){
			init=state.length>0?state[0]:''
		}
		Object.assign(this, {
			Trigger:trigger,//触发器列表
			State:state,//状态列表
			CurrentState:init,//当前状态

			_Configure:{}, //系统配置
			_ConfigureState:''//当前处于配置中的状态
		})
	}

	Configure(state) {
		this._ConfigureState=state
		Object.assign(this._Configure,{
			[state]:{
				SubstateOf:'', //指定状态的子状态
				OnEntry:()=>{
					//console.log('OnExit')
				},//进入当前状态时执行
				OnEntryFrom:[
					//{trigger:'',action:()=>{}}//从指定触发器而来时执行
				],
				Permit:[
					//{trigger:'',state:''}//当前状态下可由指定触发器转变至指定状态
				],
				OnExit:()=>{
					//console.log('OnExit')
				} //退出时执行
			}
		})
		return this
	}
	SubstateOf(state){
		this._Configure[this._ConfigureState].SubstateOf=state
		return this
	}
	OnEntry(action){
		this._Configure[this._ConfigureState].OnEntry=action
		return this
	}
	OnEntryFrom(trigger,action){
		this._Configure[this._ConfigureState].OnEntryFrom.push({trigger,action})
		return this
	}
	Permit(trigger,state){
		this._Configure[this._ConfigureState].Permit.push({trigger,state})
		return this
	}
	OnExit(action){
		this._Configure[this._ConfigureState].OnExit=action
		return this
	}
	/**
	 * 判断当前触发器是否可触发
	 */
	CanFire(trigger,state=null){
		//
		if(state==null){
			state=this.CurrentState
		}
		if(state.toString().length>0&this._Configure.hasOwnProperty(state)){
			let {SubstateOf,Permit}=this._Configure[state]
			let model=Permit.find((e)=>{
				return e.trigger==trigger
			})
			if(model==null){
				model=this.CanFire(trigger,SubstateOf)
			}
			else{
				model={
					...model,
					currentState:state
				}
			}
			return model==null?false:model
		}else{
			return false
		}
	}
	setCurrentState(state){
		console.log('--->state change from '+this.CurrentState+' to '+state)
		this.CurrentState=state
	}
	getConfigure(state){
		if(this._Configure.hasOwnProperty(state)){
			return this._Configure[state]
		}else{
			return null
		}

	}
	/*
	*从触发器trigger 进入state状态时
	*/
	_StateEnter(model){
		//执行OnEntry、OnEntryFrom
		let {fireModel,data,trigger}=model
		let {state}=fireModel
		let configModel=this.getConfigure(state)
		if(configModel){
			let {OnEntry,OnEntryFrom}=configModel
			if(OnEntry){
				OnEntry(data)
			}
			let fromModel=OnEntryFrom.find((e)=>{
				return e.trigger==trigger
			})
			if(fromModel){
				let {action}=fromModel
				if(action){
					console.log('--->'+'do action from trigger '+trigger+' to state '+state)
					action(data)
				}
			}
		}

	}
	/*
	state状态退出时
	*/
	_StateExit(model){
		let {fireModel,data}=model
		//当前状态执行OnExit
		let configModel=this.getConfigure(this.CurrentState)
		if(configModel){
			let {OnExit}=configModel
			if(OnExit){
				OnExit(data)
			}
		}
		//判断父状态是否改变
		//执行父状态OnExit

	}
	/**
	 * 调用触发器
	 */
	Fire(trigger,data){
		let fireModel=this.CanFire(trigger)
		if(fireModel){
			this._StateExit({
				trigger,data,fireModel
			})
			let {state}=fireModel
			this.setCurrentState(state)
			this._StateEnter({
				trigger,data,fireModel
			})
		}
		else{
			console.log('!CanFire')
		}
	}
}
export default {
	/**
	 * 获取状态机
	 * @param {Array} state 状态列表
	 * @param {Array} trigger 触发器列表
	 * @param {String} 初始状态  
	 */
	getInstance(state =[],trigger =[],init=null){
		return new Stateless(state,trigger,init)
	}
};
