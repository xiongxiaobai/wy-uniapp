// #ifdef VUE3
import { toRaw ,markRaw } from 'vue' 
// #endif
// #ifndef VUE3
const toRaw=function(val){
    return val
} 
const markRaw=function(val){
    return val
} 
// #endif

export {
    toRaw,markRaw
}

