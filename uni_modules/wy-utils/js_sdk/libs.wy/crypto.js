// #ifdef VUE2
var CryptoJS = require('crypto')
// #endif
// #ifdef VUE3
import CryptoJS from 'crypto-js'
// #endif
// #ifdef VUE2
export const stringToSecureKeyBuffer=function(secretKey, desiredKeyLengthBytes) {  
    // 使用 SHA-256 哈希函数    
    const hash = CryptoJS.createHash('sha256').update(secretKey).digest();  
    // 从哈希输出中截取所需长度的密钥  
    // 注意：这里假设 SHA-256 输出（256位/32字节）足够长，可以截取  
    const keyBuffer = hash.slice(0, desiredKeyLengthBytes);  
    return keyBuffer;  
}
// 加密函数
export const encryptData=function(data,secretKey) {
	
	// console.log(CrypotJS)
    // 确保密钥长度为 32 字节（对于 AES-256）   
    const key = stringToSecureKeyBuffer(secretKey,32)  
    // 生成随机 IV  
    const iv = CryptoJS.randomBytes(16);  
    // 创建 cipher 实例  
    const cipher = CryptoJS.createCipheriv('aes-256-cbc', Buffer.from(key, 'hex'), iv);  
    // 加密  
    let encrypted = cipher.update(data, 'utf8', 'base64');  
    encrypted += cipher.final('base64');  
    // 返回 IV 和加密数据的组合  
    return `${iv.toString('base64')}:${encrypted}`;
}

// 解密函数
export const decryptData=function(encryptedData,secretKey) {
     // 将加密数据拆分为IV和加密数据
    const parts = encryptedData.split(':');
    const iv = Buffer.from(parts[0], 'base64');
    const encrypted = parts[1];
    // 确保密钥长度为32字节（对于AES-256）
    const key = stringToSecureKeyBuffer(secretKey, 32);
    // 创建解密实例
    const decipher = CryptoJS.createDecipheriv('aes-256-cbc', Buffer.from(key, 'hex'), iv);
    // 解密
    let decrypted = decipher.update(encrypted, 'base64', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}
// #endif
// #ifdef VUE3
// 将字符串密钥转换为安全密钥的 Buffer（这里简单处理为使用 CryptoJS 的 WordArray）  
export const stringToSecureKeyBuffer = function(secretKey, desiredKeyLengthBytes) {  
    // 使用 SHA-256 哈希函数  
    const hash = CryptoJS.SHA256(secretKey);  
    // 计算所需的“字”数（向上取整），因为每个字是 4 字节  
    const desiredKeyLengthWords = Math.ceil(desiredKeyLengthBytes / 4);  
    // 从哈希输出中截取所需长度的密钥（以“字”为单位）  
    // 注意：如果 desiredKeyLengthWords 大于哈希输出的长度，这将返回整个哈希输出  
    const keyBuffer = hash.clone().words.slice(0, desiredKeyLengthWords);  
    // 创建一个新的 WordArray 对象，只包含我们截取的部分  
    const truncatedHash = CryptoJS.lib.WordArray.create(keyBuffer);
    // 返回截取后的 WordArray 对象  
    return truncatedHash;  
};  
  
export const encryptData= function (data, secretKey) {  
    const keySizeBytes = 32; // AES-256 需要 32 字节（256 位）的密钥  
    const key = stringToSecureKeyBuffer(secretKey, keySizeBytes);  
    const keyHex = CryptoJS.enc.Hex.stringify(key); // 将 WordArray 转换为十六进制字符串   
    // 生成随机 IV  
    const iv = CryptoJS.lib.WordArray.random(128 / 8); // 128 位 IV，即 16 字节  
    const ivBase64 = CryptoJS.enc.Base64.stringify(iv); // 将 IV 转换为 Base64 字符串  
  
    // 加密数据  
    const encrypted = CryptoJS.AES.encrypt(data, CryptoJS.enc.Hex.parse(keyHex), {  
        iv: iv,  
        mode: CryptoJS.mode.CBC,  
        padding: CryptoJS.pad.Pkcs7  
    });  
  
    // 返回 IV 和加密数据的组合（Base64 编码）  
    return `${ivBase64}:${encrypted.toString()}`;  
} 
// 解密方法  
export const decryptData = function(encryptedData, secretKey) {  
    // 解析加密数据字符串为 IV 和密文  
    const parts = encryptedData.split(':');  
    if (parts.length !== 2) {  
        throw new Error('Invalid encrypted data format');  
    }  
    const ivBase64 = parts[0];  
    const encryptedDataBase64 = parts[1];  
    // 将 IV 和密文从 Base64 转换为 WordArray  
    const iv = CryptoJS.enc.Base64.parse(ivBase64);  
    const encryptedDataCipherParams = CryptoJS.lib.CipherParams.create({  
        ciphertext: CryptoJS.enc.Base64.parse(encryptedDataBase64)  
    });  
  
    // 注意：由于我们没有直接存储 CipherParams 对象，我们需要创建一个新的 CipherParams 对象  
    // 并手动设置其 iv 和 ciphertext 属性。但在这个案例中，我们实际上只需要 ciphertext。  
    // CryptoJS.AES.decrypt 期望第二个参数是一个 WordArray（密钥）或一个 CipherParams 对象（包含 iv 和 ciphertext）  
    // 但由于我们已经有了一个单独的 IV，我们可以直接将 ciphertext 传递给 decrypt 方法，并通过 options 传递 IV  
  
    // 将密钥转换为 WordArray  
    const key = stringToSecureKeyBuffer(secretKey, 32);  
    // 解密数据  
    const decrypted = CryptoJS.AES.decrypt({  
        ciphertext: CryptoJS.enc.Base64.parse(encryptedDataBase64)  
    }, key, {  
        iv: iv,  
        mode: CryptoJS.mode.CBC,  
        padding: CryptoJS.pad.Pkcs7  
    });  
    // 返回解密后的字符串（默认为 UTF8 编码）  
    return decrypted.toString(CryptoJS.enc.Utf8);  
}
// #endif
export const encrypt =function(data,args,field='_encryptData'){
	var _encryptData= encryptData(JSON.stringify(data),args._signature)
	return {[field]:_encryptData}
}
export const decrypt =function(data,args,field='_encryptData'){
	if(data&&data.hasOwnProperty(field)){
	   var obj=decryptData(data[field],args._signature)
	   if(obj){
		   obj=JSON.parse(obj)
		   data=Object.assign(data,obj)
	   }
	}
	return data
}