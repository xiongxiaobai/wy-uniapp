var thread = { 
	newThread:function(callback=null){
		if (plus.os.name == 'Android') {  
			var App = plus.android.runtimeMainActivity();
			var Thread = plus.ios.importClass("java.lang.Thread");
			var Runnable = plus.android.implements("java.lang.Runnable", {  
			    "run": function() {
					try {
						if(callback!=null){
							callback()
						}else{
							console.log('newThread')
						}
					}catch (e) {  
						console.log('>>>>>>>>>>>>'+JSON.stringify(e));  
					}  
				}					
			})  
			var th = new Thread(Runnable);  
			return {
				start:()=>{
					 th.start()
					 th.setDaemon(true);
				},
				sleep:(time)=>{
					Thread.sleep(time)
				}
			}
		}else{
			return {
				start:()=>{
					callback()
				},
				sleep:(time)=>{
					//Thread.sleep(time)
				}
			}
		}
	}
}
export default {
    thread
} 