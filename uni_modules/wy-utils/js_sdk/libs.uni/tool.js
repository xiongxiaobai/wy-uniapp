export const compressImage= function (path,quality=80) {
	//判断后缀名
	return new Promise((resolve, reject)=>{
		//#ifndef APP-PLUS
		resolve(path)
		//#endif
		//#ifdef APP-PLUS
		uni.compressImage({
		  src: path,
		  quality,
		  success: res => {
			   uni.saveFile({
			        tempFilePath: res.tempFilePath,
			        success: function (res) {
			          var savedFilePath = res.savedFilePath;
					  resolve(savedFilePath)
			        },
					fail() {
						resolve(path)
					}
			      });
			  
		  },
		  fail() {
		  	resolve(path)
		  }
		})
		//#endif
	})
}