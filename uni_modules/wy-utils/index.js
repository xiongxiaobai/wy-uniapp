import * as obj from './js_sdk/libs/object' 
import * as arr from './js_sdk/libs/array' 
import * as url from './js_sdk/libs/url' 
import * as types from './js_sdk/libs/types' 
import * as num from './js_sdk/libs/number' 
import * as str from './js_sdk/libs/string' 
import * as date from './js_sdk/libs/date'
import * as base64 from './js_sdk/libs/base64' 
// 原型扩展需要自行添加
import md5 from './js_sdk/libs.wy/md5'
import * as image from './js_sdk/libs.wy/image'
import files from './js_sdk/libs.wy/files'
//import http from './js_sdk/libs.wy/http'
import cache from './js_sdk/libs.wy/cache'
import * as pickFile from './js_sdk/libs.wy/pickFile'
import sqlite from './js_sdk/libs.wy/sqlite'
import stateless from './js_sdk/libs.wy/stateless'
import lodash from './js_sdk/libs.wy/lodash'
import * as thread from './js_sdk/libs.wy/thread' 
import * as vue from './js_sdk/libs.wy/vue'
//#ifdef APP-PLUS
import permission from './js_sdk/libs.uni/permission'
//#endif
import * as tool from './js_sdk/libs.uni/tool'
import {getHeight} from './js_sdk/libs.uni/system.js'
var utilscore = {
    ...obj,
    ...arr,
    ...date,
    ...str,
    ...num,
    ...types,
    ...url,
    ...base64, 
	md5,
	...image,
	files,
	//http,
	cache,
	...pickFile,
	sqlite,
	stateless,
	lodash,
	...thread,
	//#ifdef APP-PLUS
	permission,
	//#endif
	...tool,
    getHeight,
    ...vue
}
export default utilscore; 


