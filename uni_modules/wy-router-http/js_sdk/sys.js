import {getModel,getList,getData,doAction} from '../utils.js'
import {getWy,getPlatform} from '@/uni_modules/wy-core/context'
const that={
    pub_appConfig(option){
        //option.method='GET' //修改请求方法
        //option.data={} //新增参数
        
        return getModel('getAppConfig',option)
    },
    pub_checkUpdate(option){
    	console.log('====--------=pub_checkUpdate')
    	let {ac}=option
    	ac=ac?ac:'checkUpdate'
    	var wy=getWy()
    	option.data={
    		// #ifdef APP-PLUS
    		appid: plus.runtime.appid,
    		appVersion: plus.runtime.version,
    		wgtVersion: wy.wgtVersion? wy.wgtVersion:plus.runtime.version,
    		// #endif
    		package:wy.config.App.package
    	}
    	return getData(ac,option)
    },
    pub_checkUpdate_(result){ 
    	// #ifndef APP-PLUS
    	if(result&&result.hasOwnProperty('code')&&result.hasOwnProperty('msg')){
    		return result
    	}
    	return {code:0,data:result,msg:''}
    	// #endif
    	if(result){
    		let {code,url,is_silently,type}=result
    		if(code>0){
    			if (is_silently) {
    				uni.downloadFile({
    					url: url,
    					success: rs => {
    						if (rs.statusCode == 200) {
    							// 下载好直接安装，下次启动生效
    							plus.runtime.install(rs.tempFilePath, {
    								force: false
    							});
    						}
    					}
    				});
    				return;
    			}
    			const PACKAGE_INFO_KEY = '__package_info__'
    			uni.setStorageSync(PACKAGE_INFO_KEY,result)
    			uni.navigateTo({
    				url: `/uni_modules/uni-upgrade-center-app/pages/upgrade-popup?local_storage_key=${PACKAGE_INFO_KEY}`,
    				fail: (err) => {
    					console.error('更新弹框跳转失败', err)
    					uni.removeStorageSync(PACKAGE_INFO_KEY)
    				}
    			})
    		}
    	}
    	if(result&&result.hasOwnProperty('code')&&result.hasOwnProperty('msg')){
    		return result
    	}
    	else{
    		return {code:0,data:result,msg:''}
    	}
    }
    
};
export default that