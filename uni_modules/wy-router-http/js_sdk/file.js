import {getWy,getPlatform} from '@/uni_modules/wy-core/context'
import {doAction} from '../utils.js'
const that={
    pub_uploadFile(obj){
        return doAction('upload',obj)
    },
	pub_uploadFile_(res){
        let {data}=res
        res.data={
            url:data,
            fid:1
        } 
        return res
    } 
};
export default that