var isOnLaunch = true;
export default {
	onLoad() {
		// 将vk实例挂载到app.globalData上，方便在非Vue页面自身函数中调用vk实例（因为获取不到this）
		let app = getApp({ allowDefault: true });
		if (app && app.globalData && !app.globalData.vk) {
			app.globalData.vk = this.vk;
		}
		if (this.vk) {
			const url = this.vk.pubfn.getCurrentPageRoute();
			// 检测是否可以分享(小程序专属)
			this.vk.navigate.checkAllowShare({ url });
			// 检测是否需要登录，只有首次启动的页面才需要检测，其他页面通过 vk.navigateTo 跳转前会自动判断。
			if (isOnLaunch && !this.vk.checkToken() && getCurrentPages().length == 1) {
				isOnLaunch = false; // 重新标记为非首次页面
				const currentPage = this.vk.pubfn.getCurrentPage() || {};
				let pagePath = currentPage.pagePath || `/${currentPage.route}` || url;
				let fullPath = currentPage.fullPath;
				let options = currentPage.options;
				this.vk.pubfn.checkLogin({ url: pagePath, fullPath, options, isOnLaunch: true }); // 检测是否需要登录
			}
		}
	},
	created() {
		// 将vk实例挂载到app.globalData上，方便在非Vue页面自身函数中调用vk实例（因为获取不到this）
		let app = getApp({ allowDefault: true });
		if (app && app.globalData && !app.globalData.vk) {
			app.globalData.vk = this.vk;
		}
	},
	methods: {
		$getData(data, key, defaultValue) {
			let { vk } = this;
			return vk.pubfn.getData(data, key, defaultValue);
		}
	},
	beforeCreate() {
		// 将vuex方法挂在到vk实例中 beforeCreate created
		let { vk, $store } = this;
        if(!vk){
            vk=uni.wy.vk
        }
		if (typeof $store !== "undefined" && typeof vk.getVuexStore === "undefined") {
			vk.getVuexStore = function() {
				return $store;
			};
			vk.vuex = (name, value) => {
				$store.commit('updateStore', { name, value });
			};
			/**
			 * (推荐) 设置vuex
			 * vk.vuex.set('$user.userInfo.avatar', avatar);
			 */
			vk.vuex.set = (name, value) => {
				$store.commit('updateStore', { name, value });
			};
			/**
			 * (推荐) 读取vuex(具有解除对象内存印射功能，且任意一层数据为undefined，不会报错)
			 * vk.vuex.get('$user.userInfo.avatar');
			 */
			vk.vuex.get = (name, defaultValue) => {
				let value = vk.pubfn.getData($store.state, name);
				if (typeof value === "undefined") return (typeof defaultValue !== "undefined") ? defaultValue : "";
				return JSON.parse(JSON.stringify(value));
			};
			vk.vuex.getters = (name) => {
				return $store.getters[name];
			};
			/**
			 * 触发vuex的指定actions(异步)
			 * $user是模块名,addFootprint是对应模块的action名称
			 * vk.vuex.dispatch('$user/addFootprint', data);
			 */
			vk.vuex.dispatch = $store.dispatch;
			/**
			 * 触发vuex的指定mutations
			 * $user是模块名,setFootprint是对应模块的action名称
			 * vk.vuex.commit('$user/setFootprint', data);
			 */
			vk.vuex.commit = $store.commit;

			/* 另一种方式 */
			/**
			 * vk.setVuex('$user.userInfo.avatar', avatar);
			 */
			vk.setVuex = vk.vuex.set;
			/**
			 * vk.getVuex('$user.userInfo.avatar');
			 */
			vk.getVuex = vk.vuex.get;
			/* 兼容老版本 */
			vk.state = vk.vuex.get;
			try {
				if (!vk.checkToken()) {
					vk.callFunctionUtil.deleteUserInfo({
						log: false
					});
				}
			} catch (err) {}
		}
	},
	computed: {

	}
}
