export default {  
    onShow(){ 
       this._notice('onPageShow') 
    },
    onHide(){ 
        this._notice('onPageHide')
    },
    methods:{
		_wy(){
			return uni.wy
		},
		_pubfn(){
			return uni.wy.pubfn
		},
        _notice(event,data=null){  
            if(this._wyPageRef){ 
                this._wyPageRef().notice(event,data)
            }
        },
        _register(ref,name=''){
            setTimeout(()=>{
                this._wyPageRef=ref
            },0)
        },
        _locale(key,group='',isContent=true,file=''){
            return uni.wy.locale(key,isContent,group,file)
        }, 
		_localUrl(url,key=''){
			return uni.wy.localUrl(url,key)
		},
		_cdnUrl(url,rootOrKey='',styleName=''){
			return uni.wy.cdnUrl(url,rootOrKey,styleName)
		}
		
    }
    
}