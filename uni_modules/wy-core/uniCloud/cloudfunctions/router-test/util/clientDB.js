class clientDB {
	constructor() {
		const db=uniCloud.database();
		this.dbCmd=db.command
		this.config = {
			vk:null,
			util:{
				 
			}
		}
		this.init= (option) =>{
			if(option){
				let {vk}=option
				let {baseDao}=vk
				this.baseDao=baseDao //兼容原来写法
				this.config={
					vk,
					util:option 
				}
			}
		}
		this.praseSort=(orderBy)=>{
			let sortArr=[]
			if(orderBy){
				let orders=orderBy.split(',')
				for(let i=0;i<orders.length;i++){
					let item=orders[i].split(' ')
					if(item.length==2){
						sortArr.push({
							name:item[0],
							type:item[1]
						})
					}
				}
			}
			return sortArr
		}
		/*
		this.getModel=async (option,_config=null)=>{
			if(_config){
				this.init(_config)
			}
			let {vk,util,dbCmd}=this.config
			let _option={
				name:'',
				where:{
					_id: dbCmd.exists(true)
				},
				field:{
					extra: false
				},
				orderBy:{
					field:'',
					orderType:''
				},
				...option
			}
			let {name,where,field,orderBy}=_option
			let sortArr=this.praseSort(orderBy)
			let data= await vk.baseDao.findByWhereJson({
				dbName:name,
				fieldJson:field,
				whereJson:where,
				sortArr
			},util);
			return { code : 0, msg : 'ok' ,data};
		}
		
		
		*/
		this.queryAll=async (command,_config=null) => {
			this.init(_config)
			let res=await uniCloud.callFunction({
				name: 'uni-clientDB',
				data: {
					command:command 
				}
			}) 
			let {result}=res
			if(result){
				return {code : 0, msg : '',data:result};
			}
			return res;
		},
		this.getModel=async (option,_config=null) => {
			this.init(_config)
			let _option={
				name:'',
				where:{},
				field:{
					extra: false
				},
				orderBy:'',
				...option
			}
			let {name,where,field,orderBy}=_option
			let command=db.collection(name)
					.where(where)
					.field(field)
					.limit(1)
			if(orderBy&&orderBy.hasOwnProperty('field')&&orderBy.hasOwnProperty('orderType')){
				command=command.orderBy(orderBy.field,orderBy.orderType)
			}
			let data=await command.get()
			if(data.data&&data.data.length>0){
				data= data.data[0]
			}
			else{
				data=null
			}
			return { code : 0, msg : 'ok' ,data:data};
		},
		this.getList=async (option,_config=null)=>{
			this.init(_config)
			let {vk,util}=this.config
			let _option={
				name:'',
				where:{},
				field:{
					//extra: false
				},
				orderBy:'',
				p:1,
				n:1000,
				...option
			}
			let {name,where,field,p,n,orderBy}=_option
			let sortArr=this.praseSort(orderBy)
			let data = await vk.baseDao.select({
				dbName:name,
				pageIndex:p,
				pageSize:n,
				fieldJson:field,
				whereJson:where,
				sortArr:sortArr
			}, util);
			let {rows}=data
			let totalRes=await this.getCount(option)
			let total=totalRes.data.total
			return { code : 0, msg : 'ok' ,data:{list:rows,total},sortArr};
		},
		this.getCount=async (option,_config=null) => {
			this.init(_config)
			let _option={
				name:'',
				where:{},
				...option
			}
			let {name,where}=_option
			let res=await db.collection(name).where(where).count()
	 
			return {code : 0, msg : '',data:res}; 
		},
		this.insertModel=async (option,_config=null) => {
			this.init(_config)
			let _option={
				name:'',
				data:{},
				...option
			}
			let {name,data}=_option
			let res=await db.collection(name).add(data)
		 
			return {code : 0, msg : 'ok',data:res};
		},
		this.updateModel=async (option,_config=null) => {
			this.init(_config)
			let _option={
				name:'',
				where:{},
				data:{},
				...option
			}
			let {name,where,data}=_option
			let res=await db.collection(name).where(where).update(data)
			return {code : 0, msg : '',data:res};
		},
		this.deleteModel=async (option,_config=null) => {
			this.init(_config)
			let _option={
				name:'',
				where:{},
				...option
			}
			let {name,where}=_option
			let res=await db.collection(name).where(where).remove()
			return {code : 0, msg : '',data:res};
		}
	}
	 
}
module.exports = new clientDB