const modules = {};
// #ifndef VUE3
const modulesFiles = require.context('@/', true, /\.app\.config\.js$/);
modulesFiles.keys().reduce((modules, modulePath) => {
	var moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	moduleName=moduleName.split('.')[0]
	const value = modulesFiles(modulePath)
	modules[moduleName] = value.default
	return modules
}, {});
// #endif
// #ifdef VUE3
//const modulesFiles3 = import.meta.globEager('../../app.config.*.js');
const modulesFiles3 = import.meta.glob('../../app.config.*.js',{ eager: true })
for (const modulePath in modulesFiles3) {
	var moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	modules[moduleName] = modulesFiles3[modulePath].default;
}
// #endif 
export default modules 
