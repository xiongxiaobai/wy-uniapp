import {getWy,getPlatform} from '@/uni_modules/wy-core/context'
import {doAction,getData} from '../utils.js'
const that={
    pub_uploadFile(obj){
        var wy=getWy()
        let {upload}=obj
        upload=upload?upload:{}
        let {filePath,cloudPath,atc,ext}=upload
        if(!cloudPath){
            if(!atc){
            	atc='common'
            }
            if(!ext){
            	var sps=filePath.split('.')
            	if(sps.length>1){
            		ext=sps[sps.length-1]
            	}
            	if(!ext){
            		ext='png'
            	}
            }
            var t = wy.pubfn.unix();
            var time = wy.pubfn.formatTime(t,'YYYYMMDD');
            var fileid=wy.pubfn.uuid()
            if(wy.pubfn.md5){
                fileid=wy.pubfn.md5(fileid)
            }
            var fileName=atc+'/'+time+'/'+fileid+'.'+ext
            //var fileName=atc+'/'+time+'/'+wy.pubfn.md5(wy.pubfn.uuid())+'.'+ext
            obj.upload.cloudPath=fileName
        }
        return getData('',obj)
    
    }, 
    pub_uploadFile_(res){
        let {fileID,success}=res
        res=success&&fileID ?{code:0,msg:'上传成功',data:{fid:fileID,url:fileID}}:{code:1,data:'',msg:'上传失败'}
        return res
    }  
};
export default that
