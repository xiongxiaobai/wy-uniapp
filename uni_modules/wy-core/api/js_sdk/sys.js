//#ifdef APP-PLUS
import checkAppUpdate from '@/uni_modules/uni-upgrade-center-app/utils/check-update'
//#endif
import {getWy,getPlatform} from '@/uni_modules/wy-core/context'
import {getModel,getList,doAction} from '../utils.js'
const that={
	pub_checkUpdate(option){
		//#ifdef APP-PLUS
		return checkAppUpdate()
		//#endif
		//#ifndef APP-PLUS
		return new Promise((resolve, reject) => {
			reject({
				message: '请在App中使用'
			})
		})
		//#endif 
	},
    pub_appConfig(option){
		console.log('--->>>>>pub_appConfig',option)
        return getModel('appConfig',option)
    }
};
export default that