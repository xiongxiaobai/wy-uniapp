function getWy(){
    return uni.wy
};
function getPlatform(){
    //#ifdef H5
    return 'h5'
    //#endif
    //#ifdef APP-PLUS
    return 'app'
    //#endif
    //#ifdef MP-WEIXIN
    return 'mp-weixin'
    //#endif
    return 'unknow'
};
function getPlatform2(){
    //#ifdef H5
    return 'h5'
    //#endif
    //#ifdef APP-PLUS
    return uni.getSystemInfoSync().platform
    //#endif
    //#ifdef MP-WEIXIN
    return 'mp-weixin'
    //#endif
    return 'unknow'
};
function ifdef(platform){
    switch(platform){
        case 'VUE3':
           // #ifdef VUE3
           return true
           //#endif
           break
        case 'VUE2':
           // #ifdef VUE2
           return true
           //#endif
           break
        case 'H5':
            //#ifdef H5
            return true
            //#endif
            break
        case 'APP-PLUS':
            //#ifdef APP-PLUS
            return true
            //#endif
            break
        case 'APP-NVUE':
            //#ifdef APP-NVUE
            return true
            //#endif
            break 
        case 'MP-TOUTIAO':
            //#ifdef MP-TOUTIAO
            return true
            //#endif
            break
        case 'MP-WEIXIN':
            //#ifdef MP-WEIXIN
            return true
            //#endif
        case 'MP':
            //#ifdef MP
            return true
            //#endif
        default:
            break
    }
    return false
};

function ifndef(platform){
    return !ifdef(platform) 
};

export  {
   getWy,ifndef,ifdef,getPlatform,getPlatform2
}
