const sdk= {
    preloadFiles:function(preloadBase){
        var preloads=[]
        // #ifndef VUE3
        const preloadFiles=require.context('@/static/preload',true, /\.[json]|[js]$/) 
        preloadFiles.keys().map((preloadPath, index) => {
            var key=preloadPath.replace('./',preloadBase) 
            key=key.replace('@/static/preload/','')
            var data=require('@/static/preload/'+key)
            preloads.push({[key]:data})
        });
        // #endif
        // #ifdef VUE3
        const modulesFiles3 = import.meta.glob('../../static/preload/**/*.js',{ eager: true });
        for (var preloadPath in modulesFiles3) { 
            var key=preloadPath.replace('../../static/preload/',preloadBase)
            key=key.replace('@/static/preload/','')
            preloads.push({[key]:modulesFiles3[preloadPath]}) 
        } 
        const modulesFiles4 = import.meta.glob('../../static/preload/**/*.json',{ eager: true });
        for (var preloadPath in modulesFiles4) {
            var key=preloadPath.replace('../../static/preload/',preloadBase)
            key=key.replace('@/static/preload/','')
            preloads.push({[key]:modulesFiles4[preloadPath]}) 
        }  
        // #endif 
        return preloads
    }
}  
export default sdk
