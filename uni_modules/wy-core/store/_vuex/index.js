let notSaveStateKeys = ['db'];
var modules = {};
let lifeData = uni.getStorageSync('lifeData') || {};
// #ifdef VUE2
const modulesFiles = require.context('./modules', true, /\.js$/);

modulesFiles.keys().map((modulePath, index) => {
	let moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	if(notSaveStateKeys.indexOf(moduleName) === -1) {
		if(!lifeData[moduleName]) lifeData[moduleName] = {};
	}
});
// 加载modules目录下所有文件(分模块)
modules = modulesFiles.keys().reduce((modules, modulePath) => {
	const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	const value = modulesFiles(modulePath)
	modules[moduleName] = value.default
	return modules
}, {});

// #endif
// #ifdef VUE3
//const modulesFiles3 = import.meta.globEager('./modules/*.js');
const modulesFiles3 = import.meta.glob('./modules/*.js',{ eager: true })
for (const modulePath in modulesFiles3) {
	const moduleName = modulePath.replace(/^\.\/modules\/(.*)\.\w+$/, '$1')
	modules[moduleName] = modulesFiles3[modulePath].default;
}
for(let moduleName in modules){
	if(notSaveStateKeys.indexOf(moduleName) === -1) {
		if(!lifeData[moduleName]) lifeData[moduleName] = {};
	}
}
// #endif  
  
uni.setStorageSync('lifeData', lifeData);
  
export default modules