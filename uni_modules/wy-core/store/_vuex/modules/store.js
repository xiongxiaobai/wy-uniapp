/**
 * vuex 用户状态管理模块
 */
let lifeData = uni.getStorageSync('lifeData') || {};
let store = lifeData.store || {};

export default {
	namespaced: true,
	state: {
		storeData: store.storeData||{} 
         
	},
	getters: { 
        StoreData: (state) => {
        	return state.storeData
        }
	},
	mutations: {
        //{[name]:[]}
        setStoreData(state,model){  
            state.storeData=Object.assign(state.storeData,model)
            uni.wy.vk.setVuex('store.storeData', state.storeData);
        },
        delStoreData(state,name){
            if(state.storeData.hasOwnProperty(name)){
                delete state.storeData[name]
                uni.wy.vk.setVuex('store.storeData', state.storeData);
            } 
        }
		 
	},
	actions: {
        //云端和本地比对
        setStoreData({commit,getters},model){ 
            var storeData=JSON.parse(JSON.stringify(getters.StoreData)) 
            let {
                name, //订单号
                list, //云端列表
                key , //唯一标识符号字段 
                skey
            }=model
            var localData=[]
            if(storeData.hasOwnProperty(name)){
                localData=storeData[name]
            } 
            var getSkey=function(_key,_data){
                var _skey=[]
                if(uni.wy.pubfn.isArray(_key)){
                    _key.forEach((k)=>{
                        if(_data.hasOwnProperty(k) &&_data[k]!=null&& _data[k]!=''){
                            _skey.push(k+'='+_data[k])
                        }
                    }) 
                }
                else if(_data.hasOwnProperty(_key)&&_data[k]!=null && _data[_key]!=''){
                     _skey.push(_key+'='+_data[_key])
                }
                return _skey.join('&')
            }
            //服务端数据
            list.forEach((li)=>{
                var _datakey=uni.wy.pubfn.md5(JSON.stringify(li)) 
                
                var id=getSkey(skey,li)
                var index=localData.findIndex((ee)=>{
                    let {data}=ee
                    var _id=getSkey(skey,data)
                    return _id==id
                }) 
                if(index>=0){
                    let {datakey,status}=localData[index]
                    if(status=='normal'){
                        localData[index].status=datakey==_datakey?'normal':'modify' 
                    }
                    
                }else{
                    localData.push({
                        datakey:_datakey,data:li,status:'normal'
                    })
                } 
            })  
            
            //判断删除
            for(var i=0;i<localData.length;i++){
                let {data}=localData[i]
                var _id=getSkey(skey,data)
                if(_id){
                    var index=list.findIndex((ee)=>{
                        var id=getSkey(skey,ee)
                        return _id==id
                    })
                    if(index<0){
                        localData[i].status='delete'
                    } 
                }else{
                    localData[i].status='new'
                }
            } 
            commit('setStoreData',{[name]:localData})
        }, 
        delStoreData({commit},name){ 
            commit('delStoreData',name)
        },
        pushStoreData({commit,getters},model){
            var storeData=JSON.parse(JSON.stringify(getters.StoreData)) 
            let {
                key, //本地数据key
                skey, //服务端key
                name,
                data, //必填 
                status,datakey,index
            }=model 
            
            var getSkey=function(_key,_data){
                var _skey=[]
                if(uni.wy.pubfn.isArray(_key)){
                    _key.forEach((k)=>{
                        if(_data.hasOwnProperty(k) && _data[k]!=''){
                            _skey.push(k+'='+_data[k])
                        }
                    }) 
                }
                else if(_data.hasOwnProperty(_key) && _data[_key]!=''){
                     _skey.push(_key+'='+_data[_key])
                }
                return _skey.join('&')
            }
             
            index=model.hasOwnProperty('index')?parseInt(index):-1
            status=status?status:'new' //默认新增 normal
            var localData=[]
            if(storeData.hasOwnProperty(name)){
                localData=storeData[name]
            }
            var n_datakey=uni.wy.pubfn.md5(JSON.stringify(data)) 
            //本地有的数据,用于删除
            if(index>=0 && index<localData.length){
                if(status=='reset'){
                    localData.splice(index,1)
                }
                else if(status=='delete' && !getSkey(skey,data)){
                    //本地有，但是服务端没有的数据
                    localData.splice(index,1) 
                }else{
                    localData[index].data=data
                    localData[index].datakey=n_datakey  
                    localData[index].status=status
                }
                
            }
            else if(datakey){
                var sindex=localData.findIndex((e)=>{
                    return e.datakey==datakey
                })
                if(sindex>=0){
                    if(status=='delete' && !getSkey(skey,data)){
                        localData.splice(sindex,1) 
                    }else{
                        localData[sindex].data=data
                        localData[sindex].datakey=n_datakey  
                        localData[sindex].status=status
                    }
                    
                } 
            }  
            else if(status=='new'){
                localData.push({
                    datakey:n_datakey,data,status
                })
            } 
            commit('setStoreData',{[name]:localData})
        }
	}
}
