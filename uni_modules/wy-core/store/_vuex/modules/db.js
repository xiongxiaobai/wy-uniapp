import {getWy} from '@/uni_modules/wy-core/context'
const state={
	tables:{
		singleTable:{
			active:false,
			option:{
				table:'single_data',
				sql://'DROP TABLE IF EXISTS single_data;'+
					'CREATE TABLE IF NOT EXISTS "single_data" ('
					+'"key"  CHAR(50) NOT NULL,'
					+'"uid"  INTEGER DEFAULT 0,'
					+'"data"  TEXT,'
					+'"dateline"  INTEGER DEFAULT 0,'
					+' PRIMARY KEY ("key")'
					+');'
			}
		},
		listTable:{
			active:false, 
			option:{
				table:'list_data',
				sql://'DROP TABLE IF EXISTS list_data;'+
					'CREATE TABLE IF NOT EXISTS list_data ('
					+'"id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,' //自增
					+'"uid"  INTEGER DEFAULT 0,'            //用户id
					+'"key"  CHAR(50) NOT NULL,'            // 唯一key
					+'"datakey"  CHAR(50),'             //本地key
					+'"data"  TEXT NOT NULL,'           //正文
					+'"list"  INTEGER DEFAULT 0,'       //排序字段
					+'"dateline"  INTEGER DEFAULT 0' //创建时间
					+');'
			}
		},
		taskTable:{
		 	active:false, 
		 	option:{
		 		table:'task_data',
		 		sql://'DROP TABLE IF EXISTS task_data;'+
					'CREATE TABLE IF NOT EXISTS task_data ('
		 			+'"id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
					+'"uid"  INTEGER DEFAULT 0,'
		 			+'"key"  CHAR(50) NOT NULL,'
					+'"datakey"  CHAR(50),'
		 			+'"data"  TEXT NOT NULL,'
		 			+'"list"  INTEGER DEFAULT 0,'
		 			+'"dateline"  INTEGER DEFAULT 0,'
		 			+'"state"  INTEGER DEFAULT 0'
		 			+');' 
		 	}
		 } 
	},
	isInit:false
};
const getters={
	SingleTableName:state=>{
		return state.tables.singleTable.option.table
	},
	ListTableName:state=>{
		return state.tables.listTable.option.table
	},
	TaskTableName:state=>{
		return state.tables.taskTable.option.table
	}
};
const mutations={};
const actions={
	//初始化
	async DBInit({state}){
		if(state.isInit){
			return
		}
		state.isInit=true
		var DB=getWy().db
		//创建数据库文件
		for(let key in state.tables){
			if(!state.tables[key].active){
				let res=await DB.createTable(state.tables[key].option)
				if(res){
					let {ret,msg}=res
					state.tables[key].active=ret
				}
			}
		} 
	},
	async getModel({getters,dispatch},_option){
		var DB=getWy().db
		dispatch('DBInit')
		let option={
			table:getters.listTable
		}
		option={...option,..._option}
		return new Promise((resolve, reject)=>{
			DB.getModel(option).then((res)=>{
				const {ret,msg,data}=res
				if(ret){
					if(data&&data.length>0){
						resolve(data[0])
					}else{
						resolve(data)
					}
				}else{
					reject(msg)
				}
			}).catch((e)=>{
				reject(e.errMsg)
			})
		})
	},
	async getList({getters,dispatch},_option){
		var DB=getWy().db
		dispatch('DBInit')
		let option={
			table:getters.ListTableName
		}
		option={...option,..._option}
		return new Promise((resolve, reject)=>{
			DB.getList(option).then((res)=>{
				const {ret,msg,data}=res
				if(ret){
					resolve(data)
				}else{
					reject(msg) 
				}
			}).catch((e)=>{
				reject(e.errMsg)
			})
		})
	},
    async insertModel({getters,dispatch},_option){
		var DB=getWy().db
		dispatch('DBInit')
		let option={
			table:getters.listTable
		}
		option={...option,..._option}
		return new Promise((resolve, reject)=>{
			DB.insertModel(option).then((res)=>{
				const {ret,msg,data}=res
				if(ret){
					resolve(data)
				}else{
					reject(msg)
				}
			}).catch((e)=>{
				reject(e.errMsg)
			})
		})
	},
	async updateModel({getters,dispatch},_option){
		var DB=getWy().db
		dispatch('DBInit')
		let option={
			table:getters.listTable
		}
		option={...option,..._option}
		return new Promise((resolve, reject)=>{
			DB.updateModel(option).then((res)=>{
				const {ret,msg,data}=res
				if(ret){
					resolve(data)
				}else{
					reject(msg)
				}
			}).catch((e)=>{
				reject(e.errMsg)
			})
		})
	},
	async deleteModel({getters,dispatch},_option){
		var DB=getWy().db
		dispatch('DBInit')
		let option={
			table:getters.listTable
		}
		option={...option,..._option}
		return new Promise((resolve, reject)=>{
			DB.deleteModel(option).then((res)=>{
				const {ret,msg,data}=res
				if(ret){
					resolve(data)
				}else{
					reject(msg)
				}
			}).catch((e)=>{
				reject(e.errMsg)
			})
		})
	}
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}