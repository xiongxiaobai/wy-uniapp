import {getWy,getPlatform} from '@/uni_modules/wy-core/context'
const state={
	
};
const getters={};
const mutations={};
const actions={
	async UploadFile({},option){
		/*option.data={
			filePath,fileType,atc,uploadTask,success 
		}*/ 
		return getWy().callFunction({
			url:'uploadFile',//file/kh/
            upload:option
		}) 
	 }
	
	
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}