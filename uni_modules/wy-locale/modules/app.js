export default {
	keys:['zh-Hans','en','es'],//中文 英文 西班牙语
	lang:{
		login:{
			_translate:[
				['注册账号','register','Registro'],
				['用户名','username','Nombre de usuario'],
				['找回密码','remember','Recuperar'],
				['和','and','Y'],
				['获取','get code','Obtener'],
				['已读且同意','Read and agreed','Leído y acordado'],
				['登录','login in','Iniciar sesión'],
				['手机登录','mobile','Móvil'],
				['账号登录','account','Cuenta'],
				['密码','password','Palabras clave'],
				['验证码','verification code','Código de verificación'],
				['《用户注册协议》','《Registet license》','《Licencia regiset》'],
				['《隐私政策》','《Privacy policy》','《Política de privacidad》'],
				['请输入正确的手机号码','Please enter the correct mobile number','Introduzca el número de teléfono correcto'],
				['请输入手机号','enter your mobile number','Introduzca su número de teléfono móvil'],
				['请输入短信验证码','enter SMS verification code','Introduzca el Código de verificación SMS'],
				['验证码已发送','Verification code sent','Código de verificación enviado'],
				['账号不能为空','account number cannot be empty','La cuenta no puede estar vacía'],
				['验证码不能为空','verification code cannot be empty','El Código de verificación no puede estar vacío'],
				['密码不能为空','password cannot be empty','La contraseña no puede estar vacía'],
				['登陆成功','Login successful','Inicio de sesión exitoso'],
				['请输入验证码','Please enter the verification code','Introduzca el Código de verificación'],
				['登录中...','Logging in ...','Iniciar sesión']
			],
			register:['注册账号','register','register'],
			username:['用户名','username','username']
		},
		wyModal:{
			_translate:[
				['提示','Tips','Prompt'],
				['确定','sure','Determinar'],
				['取消','cancel','Cancelar'],
				['是','yes','Sí.'],
				['否','no','No.'],
				['该操作需要登录,是否立即登录?','This operation requires login. Do you want to login now?','Esta operación requiere inicio de sesión. Desea iniciar sesión ahora?']
			]
		}
	},
	translate:[
		['暂无介绍','No introduction','No Introduction'],
		['点击登陆','Click login','Haga clic en inicio de sesión'],
		['退出账号','Exit account','Número de cuenta de salida'],
		['检查更新中...','Checking for updates ...','Comprobar actualizaciones ...'],
		['已经是最新版本!','It is the latest version!','¡Es la última versión!'],

		['请求中...','loading...','Solicitud...'] 
	]
}
