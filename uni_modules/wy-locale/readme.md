# wy-locale uniapp国际化方案
* 本方案只是针对uniapp现有的国际方案的补充优化
* 涉及到pages.json manifest.json 等框架内置文件仍使用官方方案
* 已在安卓/iOS/iPad均有应用，效果还可以，就是不知道对性能是否有影响或者有优化对空间，欢迎留言。
# 本方案特点
* 支持中文或者指定语言作为键值
* 多语言配置在一个文件如 lang:['中文','english','es']
* 支持分组词库、公共词库。传入分组key值后优先按分组匹配，其次以公共词库兜底

# 目录结构
```
├── wy-locale
│   ├── modules
|	|	├──app.js
|	|	├──demo.js
│   ├── utils
|	|	├── md5.js
│   └── index.js
```


# 使用教程参考样例工程
## 词库文件demo.js参考配置
```
export default {
	keys:['zh-Hans','en','es'],//中文 英文 西班牙语
	lang:{
		nvue:{
			_translate:[ //分组等公共词库
				['分组匹配','group','group-es'],
				['你好','hello','HI'],
				['分组优先匹配','group first 233','group first es 344'] ,//公共词库优先级最低
				['分组公共词库','group public']
			],
			//完整键值 demo.nvue.register
			demokey:['key值精准匹配，需传入文件名作为分组','key demo','key demo-es'],
			groupkey:['分组优先匹配','group first','group first es']
		},
		com:{ //组件
			_translate:[
				['组件分组匹配','com group','group-es'], 
				['组件分组优先匹配','com group first 233','group first es 344'] ,//公共词库优先级最低
				['组件分组公共词库','com group public']
			],
			demokey:['key值精准匹配，需传入文件名作为分组','key demo','key demo-es'],
			groupkey:['分组优先匹配','group first','group first es']
		},
		locale:{
			auto:['系统','auto'],
			en:['英文','english'],
			'zh-hans':['中文简体','zh-hans'],
			
		}
	},
	translate:[ //公共词库，中文作为键值
		['中文公共词库匹配','use zh-cn'],
		['wy-locale补充方案','lala'],
		['语言','language'],
		['应用此设置将重启App','Applying this setting will restart the app']
	]
}


```
## main.js 初始化
```
import wyLocale from '@/uni_modules/locale/index'
import locales from '@/locale/custom' //自定义词库，同名键值可以覆盖插件默认词库
let {localeParse,localeInit}=wyLocale
var messageObj=localeParse([locales],'zh-Hans') //使用中文作为键值
uni.$wyLocale=wyLocale.localeInit(messageObj)  //挂载在uni下

```
## 使用方法，在页面和组件等methods中定义方法
```
/**
* 在methods 中定义方法
* @param {*} key 值
* @param {*} isContent 是否传入内容作为键值
* @param {*} group 分组
*/
localeFun(key,isContent=true,group='nvue'){
  return uni.$wyLocale(key,isContent,group) 
},

```

## 页面中使用，支持nvue
```
<view class="title">{{localeFun('中文公共词库匹配',true,'')}}</view>
<view class="title">{{localeFun('分组优先匹配',true,'nvue')}}</view>
<view class="title">{{localeFun('分组公共词库',true,'nvue')}}</view>
<view class="title">{{localeFun('nvue.demokey',false,'demo')}}</view>
<view class="title">{{localeFun('匹配不到原样输出',true,'nvue')}}</view>
<wy-locale-com></wy-locale-com>
```

## 组件中使用
```
<view class="title">{{localeFun('组件分组优先匹配',true,'com')}}</view>
<view class="title">{{localeFun('组件分组公共词库',true,'com')}}</view>
<view class="title">{{localeFun('com.demokey',false,'demo')}}</view>
```

## js中使用直接调用方法即可
详情参考样例方法。
[在线文档地址](https://doc.weiyunkj.com/core/fun/locale.html)