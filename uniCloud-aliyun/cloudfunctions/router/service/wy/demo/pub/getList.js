'use strict';
module.exports = {
	/**
	 * 此函数名称
	 * @url user/pub/test1 前端调用的url参数地址
	 * data 请求参数
	 * @param {String} params1  参数1
	 */
	main: async (event) => {
		let { data = {}, userInfo, util, originalParam } = event;
		let { customUtil, config, pubFun, vk, db, _ } = util;
		let { uid } = data;
		let res = { code: 0, msg: "获取列表数据" };
		// 业务逻辑开始-----------------------------------------------------------
		// 可写与数据库的交互逻辑等等

        res.data={
            list:[
                {title:'1'},
                {title:'2'},
                {title:'3'},
                {title:'4'},
                {title:'5'},
            ],
            total:10
        }


		// 业务逻辑结束-----------------------------------------------------------
		return res;
	}
}
