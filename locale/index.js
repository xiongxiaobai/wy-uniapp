const modules={}
// #ifndef VUE3
const modulesFiles = require.context('./modules', true, /\.js$/);
modulesFiles.keys().reduce((modules, modulePath) => {
	const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
	const value = modulesFiles(modulePath)
	modules[moduleName] = value.default
	return modules
}, {});
// #endif
// #ifdef VUE3
const modulesFiles3 = import.meta.glob('./modules/*.js',{ eager: true })
//const modulesFiles3 = import.meta.globEager('./modules/*.js');
for (const modulePath in modulesFiles3) {
	const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1').replace('modules/','') 
	modules[moduleName] = modulesFiles3[modulePath].default;
}
// #endif
export default modules