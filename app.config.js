// 引入自定义公共函数
import myPubFunction from '@/common/function/myPubFunction.js'
import formApi from '@/common/api/wyform/index.js' 
//注意，如果接口使用框架内置的加密解密算法，在vue3环境下，需要`npm install crypto-js@3.1.9-1`,务必安装此版本，版本太高会报错。
import {encrypt,decrypt} from '@/uni_modules/wy-utils/js_sdk/libs.wy/crypto.js'
export default {
	wyConfig:{ 
		Sys:{
			isTest:true,//process.env.NODE_ENV !== 'production',
			isRequest:true,//默认请求
			//interceptor:'demo,wyapi,unicloud',//拦截器列表
			interceptor:'unicloud,formApi',//拦截器列表unicloud
			wss:{
				host:''
			},
            map:{
                amap:{
                    key:'',
                    //serviceHost:'https://wantong1.jixiangche.net/_AMapService',
                }      
            },
            uniapis:{ //
                base:'router',
                test:'router',
            	keys:{
                    user:'user',
                    demo:'wy/demo'
            	},
            },
			apis:{//api接口地址
				base:'https://api.biaogedan.com/?s=app.core.',
				test:'https://wy.api.jumpurl.cn/?s=app.core.', 
				keys:{
					user:'user&ac=', 
                    api:'api&ac=', 
                    sys:'sys/', 
				}
			},
			cdn:{
				host:'http://cnd.biaogedan.com/',
				local:'cnd.biaogedan.com',
                keys:{
                   file:'myfile', //实际地址为http://cnd.xxx.com/myfile
                   avatar:'https://avatar.cdn.com', //实际地址为https://avatar.cdn.com
                   local:'@/static/local/',  //强制将该分组资源映射到一个本地文件夹
                   test:'#/static/logo.png'  //强制将该分组所有资源替换为一个本地文件，用于测试 
                },
				style:{ 
                    cover:'?imageView2/0/format/jpg/q/75',
				}
			},
			h5:{ //嵌入h5页面

			}
		},
		Interceptor:{
            formApi:{
                actions:formApi,
                _isRESTful:true, 
                _autoMatch: true,
                _isRequest: true,
                _isPreload:true,
                _data:{
                  
                },
				_encrypt:encrypt,//加密解密
				_decrypt:decrypt,//加密解密
                filters:{
                	'checkUpdate':'sys.pub.checkUpdate',
                	'appConfig':'sys.pub.appConfig', 
                },
                _checkToken(res,vk){ 
                   // console.log(res)
                	let {msg,code,ret}=res 
                	return ret===100||ret==-1
                },
            },  
            unicloud:{
                filters:{ 
                	'file/kh/uploadFile':'file.kh.uploadFile'
                },
            }
		},
		Local:{
            res:{
                base:'/static/',
                keys:{
                    icon:'icon',//公共图标 @icon #icon/xx.png
                    imgs:'imgs',//图片
                    user:'apps/user',//用户模块专用资源目录
                },
                data:{
                    logo:'logo.png',
                    banner:'#/static/demo/banner.jpg',
                    tabbar:{ 
                       index:'index.png'
                    } 
                }
            },
            config:{
              key:'WY_CONFIG_KEY' //允许本地保存配置  
            }
            
		},
		Router:{
			//#ifdef MP-WEIXIN
			//base:'/uniSubpackage',
			//#endif
			root:'/pages/',
            index:'#/pages/index/index', //首页 //#绝对定位到文件 带@则组装路由时会忽略分组名
            login:'#/pages/login/login', //登录页 
            web:'#/pages/web/web',  //内部webview 
            tabbar:'@/pages/index/',
            keys:{
               feed:'#/pages/index/feed',
               erp:'/pages/apps/',
               demo:'/pages/apps/', 
			   components:'@/uni_modules/wy-page-app/pages/demo/',
			   framework:'@/uni_modules/wy-core/pages/demo/',
            },
            rules:{
                'https://biaogedan.com/#/pages/':'/pages/'
            }
		},
		Form:{
		    components:{
               demo:'@/uni_modules/wy-form/components/wy-form-demo/wy-form-demo.vue' 
            } //自定义组件
		},
        Theme:{
            rgshop:{
                head:{
                    backgroundColor:'#e54d42', 
                    frontColor:'#ffffff'
                },
                page:{
                    
                } 
            }
        },
        App:{ 
           // theme:'rgshop',
			logo:'/static/logo.png',
			sitename:'wy',
			siteurl:'http://biaogedan.com',//站点url
			sitekey:'biaogedan',  //站点
			package :'com.biaogedan.wyformApp' //包名
		}
	},
	
	//*************以下兼容vk配置文件*****************//
	//locale:'zh-Hans',
	// 开发模式启用调式模式(请求时会打印日志)
	debug: process.env.NODE_ENV !== 'production',
	// 主云函数名称
	functionName: "router", 
	// 登录页面路径
	login: {
	  url: '/pages/login/login'
	},
	// 首页页面路径
	index: {
	  url: '/pages/index/index'
	},
	// 404 Not Found 错误页面路径
	error: {
	  url: '/pages/error/404/404'
	},
	// 日志风格
	logger: {
	  colorArr: [
	    "#0095f8",
	    "#67C23A"
	  ]
	},
	/**
	 * app主题颜色
	 * vk.getVuex('$app.config.color.main')
	 * vk.getVuex('$app.config.color.secondary')
	 */
	color: {
	  main: "#ff4444",
	  secondary: "#555555"
	},
	// 需要检查是否登录的页面列表
	checkTokenPages: {
	  /**
	   * 如果 mode = 0 则代表自动检测
	   * 如果 mode = 1 则代表list内的页面需要登录，不在list内的页面不需要登录
	   * 如果 mode = 2 则代表list内的页面不需要登录，不在list内的页面需要登录
	   * 注意1: list内是通配符表达式，非正则表达式
	   * 注意2: 需要使用 vk.navigateTo 代替 uni.navigateTo 进行页面跳转才会生效
			 * 注意3: 首次进入的页面暂无法检测，故不会生效。
			 * 但只要页面上执行kh或sys函数，会自动判断是否登录，未登录也会自动跳登录页面，登录成功后会自动返回本来要跳转的页面。
	   */
	  mode: 0,
	  list: [
	    "/pages/*",
	    "/pages/login/*",
	    "/pages/index/*", 
	    "/pages/error/*"
	  ]
	},
	// 需要检查是否可以分享的页面列表(仅小程序有效)
	checkSharePages: {
	  /**
	   * 如果 mode = 0 则不做处理
	   * 如果 mode = 1 则代表list内的页面可以被分享，不在list内的页面不可以被分享
	   * 如果 mode = 2 则代表list内的页面不可以被分享，不在list内的页面可以被分享
	   * 注意: list内是通配符表达式，非正则表达式
	   */
	  mode: 0,
	  // ['shareAppMessage', 'shareTimeline'],
	  menus: ['shareAppMessage'],
	  list: [
	    "/pages/index/*",
	    "/pages/goods/*",
	    "/pages_template/*",
	  ]
	},
	// 静态文件的资源URL地址
	staticUrl: {
	  // Logo
	  logo: '/static/logo.png' 
	},
	// 自定义公共函数，myPubFunction内的函数可通过vk.myfn.xxx() 调用
	myfn: myPubFunction,
	// 第三方服务配置
	service: {
	  // 密钥和签名信息 (由于签名的获取比较麻烦,建议初学者使用上传到unicloud的方案,上传到阿里云OSS是给有特殊需求的用户使用)
	  // 相关文档 : https://help.aliyun.com/document_detail/31925.html?spm=a2c4g.11186623.6.1757.b7987d9czoFCVu
	  aliyunOSS: {
	    // 密钥和签名信息
	    uploadData: {
	      OSSAccessKeyId: "",
	      policy: "",
	      signature: "",
	    },
	    // oss上传地址
	    action: "https://xxx.oss-cn-hangzhou.aliyuncs.com",
	    // 根目录名称
	    dirname: "test",
	    // oss外网访问地址，也可以是阿里云cdn地址
	    host: "https://xxx.xxx.com",
	    // 上传时,是否按用户id进行分组储存
	    groupUserId: true,
	    // 是否默认上传到阿里云OSS
	    isDefault: false
	  }
	},
	// 全局异常码，可以自定义提示结果
	globalErrorCode: {
	  // 阿里云10秒非正常超时，其实请求还在执行（且一般已经成功了，但前端接接受不到成功结果）
	  "cloudfunction-unusual-timeout": "请求超时，但请求还在执行，请重新进入页面。",
	  // 请求超时（真正的请求超时）
	  "cloudfunction-timeout": "请求超时，请重试！",
	  // 不在预期内的异常（如数据库异常、云函数编译异常等）
	  "cloudfunction-system-error": "网络开小差了！"
	}
}
