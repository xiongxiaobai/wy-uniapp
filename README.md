# 未耘开发框架
- `起步`，主要介绍本框架的安装、引用流程，并分享了个人对于uniapp开发的理解
- `配置`，介绍了框架依赖的配置项信息
- `基础`，介绍了框架封装的实用工具方法
- `进阶`，结合具体的业务模块进行封装，并介绍使用方法
- `高级`，主要介绍一些原生插件的使用
## 框架特色
- 全面适用于nvue、vue/vue3、unicloud、小程序等开发场景。从0到上线功能完整，适用新手入坑。
- 独创接口预加载功能，可将本地json/js文件作为接口优先数据，实现uniapp的mock功能。
- 独创接口缓存功能，满足离线缓存、字典缓存的需求，同时可以将缓存作为初始展示数据，加强用户体验。
- 独创的[发布订阅模式](/core/fun/publish)，在跨页面通讯、wss全局通知等方面得到了非常棒的体验。
- 独创接口请求范式，可按模块分组，并自定义接口基地址、header、data、接口拦截、结果包装、RESTful风格、是否云函数等。
- 独创路由分组功能，用于满足模块化功能的移植和任意组合。
- 原创国际化方案插件[wy-locale](https://ext.dcloud.net.cn/plugin?id=8465)，详情见插件说明。
- 整体框架重逻辑轻UI，内置组件几乎类似官方说的无渲染组件。内置丰富js工具库，并封装了缓存、文件操作、图片处理、sqlite操作、状态机等实用功能。
- css设计上考虑了多主题、多尺寸，博采众长，基本满足所需。同时内置了用于适配colorui、npro、uview、uni-ui的scss，可以任意采用以上组件库来实现项目的UI部分。
- 完全兼容[vk-unicloud-router](https://ext.dcloud.net.cn/plugin?id=2204)，不做云开发也可以使用vk大部分功能。
- 框架由多个符合uni_modules规范的内部插件组成，也便于兼容插件市场其它插件。
- 其它只可意会的功能。并非只把客户端当中一个展示前端页面的“浏览器”，而是会承担一些客户端必要的逻辑运算。
